package tostringexamples;

public class Mobile {

	String brand;
	double price;

	Mobile(String brand, double price) {
		this.brand = brand;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Mobile [price=" + price + "]";
	}

	public static void main(String[] args) {
		Mobile m = new Mobile("Apple", 56000);
		System.out.println(m); // Apple 56000.0

		Mobile m2 = new Mobile("Samsung", 34000);
		System.out.println(m2);
	}
}
