import java.util.Scanner;

public class SampleDoWhileLoopExample {
	public static void main(String[] args) {
		// creating scanner object outside the loop
		Scanner s = new Scanner(System.in);

		do {
			System.out.println("Enter two numbers");
			int num1 = s.nextInt();
			int num2 = s.nextInt();
			System.out.println(num1 + num2);
			System.out.println("Do you want to add more numbers? type true/false");
		} while (s.nextBoolean());
		/*
		 * when the user enter true, the loop executes again and when the user enter false,
		 * the program terminates
		 */
	}
}
