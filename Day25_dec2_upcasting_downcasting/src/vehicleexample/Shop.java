package vehicleexample;

public class Shop {

	// sell is an instance method of the shop class,
	// returns Vehicle if option is 1=>Car 2=>Bike 3=>Truck
	Vehicle sell(int option) {
		if (option == 1) {
			Car c = new Car();
			c.color = "Black";
			c.brand = "Mercedes";
			c.isAutomatic = true;
			c.price = 5000000;
			return c;
		} else if (option == 2) {
			Bike b = new Bike();
			b.color = "Blue";
			b.brand = "BMW";
			b.isKickerAvailable = true;
			b.price = 2000000;
			return b;
		} else {
			Truck t = new Truck();
			t.color = "Brown";
			t.brand = "TATA";
			t.loadCapacity = 2500;
			t.price = 400000;
			return t;
		}
	}

	public static void main(String[] args) {
		Shop s = new Shop();

		Vehicle v = s.sell(2); // Vehicle v = new Car();
		System.out.println(v.color);
		System.out.println(v.brand);
		System.out.println(v.price);
		v.start();
		v.stop();

		if (v instanceof Car c) { // downcasting
			System.out.println(c.isAutomatic);
			c.cruizeControl();
		} else if (v instanceof Bike b) {
			System.out.println(b.isKickerAvailable);
			b.kick();
		} else if (v instanceof Truck t) {
			System.out.println(t.loadCapacity);
			t.dropLoad();
		}
	}

}
