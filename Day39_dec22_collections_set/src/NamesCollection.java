import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class NamesCollection {
	public static void main(String[] args) {
		ArrayList<String> namesList = new ArrayList<>(Arrays.asList("Mark", "Alpha", "Delta", "Charlie", "Roger"));

		Iterator<String> itr = namesList.iterator();
		while (itr.hasNext()) {
			String x = itr.next();
			System.out.println("Removing element " + x);
			itr.remove();
		}
		System.out.println(namesList); // []

	}
}
