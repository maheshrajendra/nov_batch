import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SampleList {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 4, 6, 3, 2, 7, 6, 8, 9, 3, 43, 23, 45));
		List<Integer> minList = list.stream().filter(e -> check(e.intValue())).collect(Collectors.toList());
		List<Integer> oddNumber = list.stream().filter(e -> e % 2 != 0).collect(Collectors.toList());
		System.out.println(minList);
		System.out.println(oddNumber);
	}

	static private boolean check(int i) {
		return i < 10;
	}
}
